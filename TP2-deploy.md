# TP2 : Déploiement automatisé

## I. Déploiement simple

Créer un Vagrantfile qui :

- utilise la box centos/7
- crée une seule VM

  - avec 1Go RAM
  - une IP statique `192.168.2.11/24`
  - mettre un nom (pour Vagrant)
  - un hostname

  ```ruby
  Vagrant.configure("2")do|config|
      config.vm.box="centos/7"

      # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
      config.vbguest.auto_update = false

      # Désactive les updates auto qui peuvent ralentir le lancement de la machine
      config.vm.box_check_update = false

      # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
      config.vm.synced_folder ".", "/vagrant", disabled: true


      config.vm.define "tp2" do |tp2|


          tp2.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"

          tp2.vm.provider "virtualbox" do |vb|

              vb.name = "TP2_B2"

              vb.memory = "1024"

              vb.hostname = "node"
          end
      end
  end
  ```

Modifier le Vagrantfile

- la vm exécute un script shell au démarrage qui install le paquet vim
  ```
  tp2.vm.provision "shell", path: "script.sh"
  ```
  Contenu de script.sh
  ```
  yum install vim -y
  ```
- Pas reussi à mettre le second disk..

## II. Re-package

Pour repackager une box, qu'on appelera b2-tp2-centos en partant de la box centos/7, il faut :

- une mise à jour système
  ```
  yum update
  ```
- l'installation de paquets additionels
  ```
  yum install vim epel-release nginx
  ```
- désactivation de SELinux

  `sudo setenforce 0`

- firewall (avec firewalld, en utilisant la commande firewall-cmd)
  - activé au boot de la VM
    ```
    sudo systemctl enable firewalld
    ```
  - ne laisse passser que SSH
    ```
    sudo firewall-cmd --add-port=22/tcp --permanent
    sudo firewall-cmd --reload
    ```
- Repackage

```
vagrant package --output b2-tp2-centos.box
vagrant box add b2-tp2-centos b2-tp2-centos.box
```

## III. Multi-node deployment

Il faut ensuite créer un Vagrantfile qui lance deux machines virtuelles, les VMs doivent utiliser votre box repackagée comme base.


Le Vagrantfile :

```
Vagrant.configure("2")do|config|
    config.vm.box="b2-tp2-centos"

    # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
    config.vbguest.auto_update = false

    # Désactive les updates auto qui peuvent ralentir le lancement de la machine
    config.vm.box_check_update = false

    # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
    config.vm.synced_folder ".", "/vagrant", disabled: true

    config.vm.define "node1" do |node1|
        node1.vm.hostname = "node1.tp2.b2"
        node1.vm.network "private_network", ip: "192.168.2.21", netmask:"255.255.255.0"
        node1.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
        end
    end

    config.vm.define "node2" do |node2|
      node2.vm.hostname = "node2.tp2.b2"
      node2.vm.network "private_network", ip: "192.168.2.22", netmask:"255.255.255.0"
      node2.vm.provider "virtualbox" do |vb|
        vb.memory = "512"
      end
    end
end
```

### IV. Automation here we (slowly) come
Créer un Vagrantfile qui automatise la résolution du TP1

Vagrant file

```rubis
Vagrant.configure("2")do|config|
  config.vm.box="b2-tp2-centos"

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false

  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.define "node1" do |node1|
      node1.vm.hostname = "node1.tp2.b2"
      node1.vm.network "private_network", ip: "192.168.1.11", netmask:"255.255.255.0"
      node1.vm.provider "virtualbox" do |vb|
        vb.memory = "1024"
      end
  end

  config.vm.define "node2" do |node2|
    node2.vm.hostname = "node2.tp2.b2"
    node2.vm.network "private_network", ip: "192.168.1.12", netmask:"255.255.255.0"
    node2.vm.provider "virtualbox" do |vb|
      vb.memory = "512"
    end
  end
end
```
Toujours des difficultés avec le scripting..